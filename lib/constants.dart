import 'package:flutter/material.dart';

const kColorizeAnimationColors = [
  Colors.red,
  Colors.cyanAccent,
  Colors.blue,
  Colors.lightGreenAccent,
  Colors.yellow,
];

const kGradientColors = [Color(0xff1542bf), Color(0xff51a8ff)];

const kAnimationTextStyle = TextStyle(
  fontSize: 50,
  fontWeight: FontWeight.bold,
  fontFamily: 'Lobster',
);

const kTimerTextStyle = TextStyle(
  fontWeight: FontWeight.bold,
  fontSize: 36,
  fontFamily: 'Lobster',
);

const KTapToStartTextStyle = TextStyle(
    fontSize: 27,
    fontWeight: FontWeight.bold,
    color: Colors.black);

const kScoreLabelTextStyle = TextStyle(
  fontSize: 28,
  color: Colors.orangeAccent,
  fontFamily: 'Press_Start_2P',
);

const kScoreIndicatorTextStyle = TextStyle(
  fontSize: 42,
  color: Colors.black,
  fontFamily: 'Chakra_Petch',
  fontWeight: FontWeight.bold,
);

const kQuizTextStyle = TextStyle(
  fontSize: 30,
  color: Colors.white60,
  fontFamily: 'Architects_Daughter',
);

const kButtonTextStyle = TextStyle(
  fontSize: 40,
  fontFamily: 'Press_Start_2P',
);

const kTitleTS = TextStyle(
  fontSize: 32,
  color: Colors.black,
  fontFamily: 'Press_Start_2P',
);

const kContentTS = TextStyle(
  fontSize: 24,
  color: Colors.black,
  fontWeight: FontWeight.bold,
);

const kDialogButtonsTS = TextStyle(
  fontSize: 18,
  color: Colors.black,
);