import 'dart:async';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:math_quiz_app/constants.dart';
import 'package:math_quiz_app/quizBrain.dart';
import 'package:math_quiz_app/screens/welcome_screen.dart';
import 'package:outline_gradient_button/outline_gradient_button.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:shared_preferences/shared_preferences.dart';

// WelcomeScreen _welcomeScreen = WelcomeScreen();
QuizBrain _quizBrain = QuizBrain();
int _score = 0;
int _highScore = 0;
double _value = 0;
int _falseCounter = 0;
int _totalNumberOfQuizzes = 0;

class GameScreen extends StatefulWidget {
  static final id = 'game_screen';

  @override
  _GameScreenState createState() => _GameScreenState();
}

class _GameScreenState extends State<GameScreen> {
  late Timer _timer;
  int _totalTime = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    startGame();
  }

  void startGame() async {
    _quizBrain.makeQuiz();
    startTimer();
    _value = 1;
    _score = 0;
    _falseCounter = 0;
    _totalNumberOfQuizzes = 0;
    SharedPreferences preferences = await SharedPreferences.getInstance();
    _highScore = preferences.getInt('highscore') ?? 0;
  }

  void startTimer() {
    const speed = Duration(milliseconds: 100);
    _timer = Timer.periodic(speed, (timer) {
      if (_value > 0) {
        setState(() {
          _value > 0.005 ? _value -= 0.005 : _value = 0;
          _totalTime = (_value * 20 + 1).toInt();
        });
      } else {
        setState(() {
          _totalTime = 0;
          showMyDialog();
          _timer.cancel();
        });
      }
    });
  }

  Future<void> showMyDialog() {
    return showDialog<void>(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(
                  25,
                )),
            backgroundColor: Colors.white,
            title: FittedBox(
              child: const Text('GAME OVER',
                  textAlign: TextAlign.center, style: kTitleTS),
            ),
            content: Text('Score: $_score | $_totalNumberOfQuizzes',
                textAlign: TextAlign.center, style: kContentTS),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.pushNamed(context, WelcomeScreen.id);
                },
                child: const Text('EXIT', style: kDialogButtonsTS),
              ),
              TextButton(
                onPressed: () {
                  startGame();
                  Navigator.of(context).pop();
                },
                child: const Text('PLAY AGAIN', style: kDialogButtonsTS),
              )
            ],
          );
        });
  }

  CircularPercentIndicator buildCircularPercentIndicator() {
    return CircularPercentIndicator(
      radius: 120.0,
      lineWidth: 12,
      percent: _value,
      circularStrokeCap: CircularStrokeCap.round,
      center: Text(
        '$_totalTime',
        style: kTimerTextStyle,
      ),
      progressColor: _value > 0.6
          ? Colors.green
          : _value > 0.3
          ? Colors.yellow
          : Colors.red,
    );
  }

  Column getPortraitMode() {
    return Column(
      children: [
        ScoreIndicators(),
        QuizBody(),
        Expanded(flex: 2, child: buildCircularPercentIndicator()),
        Expanded(
          flex: 2,
          child: Row(
            children: [
              ReUsableOutlineButton(
                  color: Colors.redAccent, userChoice: 'FALSE'),
              ReUsableOutlineButton(
                  color: Colors.lightGreenAccent, userChoice: 'TRUE'),
            ],
          ),
        ),
      ],
    );
  }

  Row getLandscapeMode() {
    return Row(
      children: [
        ReUsableOutlineButton(userChoice: 'FALSE', color: Colors.redAccent),
        Expanded(
          flex: 3,
          child: Column(
            children: [
              ScoreIndicators(),
              QuizBody(),
              Expanded(flex: 3, child: buildCircularPercentIndicator()),
            ],
          ),
        ),
        ReUsableOutlineButton(
            userChoice: 'TRUE', color: Colors.lightGreenAccent),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    var data = MediaQuery.of(context);
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: kGradientColors,
          ),
        ),
        child: getTrueMode(data),
      ),
    );
  }

  Widget getTrueMode(MediaQueryData data) {
    if (data.size.width < data.size.height)
      return getPortraitMode();
    else
      return getLandscapeMode();
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }
}

class ReUsableOutlineButton extends StatelessWidget {
  ReUsableOutlineButton({this.userChoice, this.color});

  final userChoice;
  final color;

  void playSound(String soundName) {
    final _player = AudioCache();
    _player.play(soundName);
  }

  void checkAnswer() async {
    if (userChoice == _quizBrain.quizAnswer) {
      playSound('correct-choice.wav');
      _score++;
      _value >= 0.89 ? _value = 1 : _value += 0.1;
      if (_highScore < _score) {
        _highScore = _score;
        SharedPreferences preferences = await SharedPreferences.getInstance();
        preferences.setInt('highscore', _highScore);
      }
    } else {
      playSound('wrong-choice.wav');
      _falseCounter++;
      _value < 0.02 * _falseCounter
          ? _value = 0
          : _value -= 0.02 * _falseCounter;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: OutlineGradientButton(
          padding: EdgeInsets.symmetric(horizontal: 20),
          gradient: LinearGradient(
            colors: kGradientColors,
          ),
          strokeWidth: 12,
          child: Center(
              child: FittedBox(
                child: Text(
                  userChoice,
                  style: kButtonTextStyle.copyWith(color: color),
                ),
              )),
          elevation: 1,
          radius: Radius.circular(36),
          onTap: () {
            _totalNumberOfQuizzes++;
            checkAnswer();
            _quizBrain.makeQuiz();
          },
        ),
      ),
    );
  }
}

class QuizBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 2,
      child: FittedBox(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Text(
            _quizBrain.quiz,
            style: kQuizTextStyle,
          ),
        ),
      ),
    );
  }
}

class ScoreIndicators extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 24, right: 24, top: 24),
      child: FittedBox(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            ScoreIndicator(label: 'HIGHSCORE', score: '$_highScore'),
            SizedBox(width: 40),
            ScoreIndicator(label: 'SCORE', score: '$_score'),
          ],
        ),
      ),
    );
  }
}

class ScoreIndicator extends StatelessWidget {
  ScoreIndicator({this.label, this.score});

  final label;
  final score;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(label, style: kScoreLabelTextStyle),
        SizedBox(height: 10),
        Text(score, style: kScoreIndicatorTextStyle),
      ],
    );
  }
}
