import 'package:flutter/material.dart';
import 'package:math_quiz_app/screens/game_screen.dart';
import 'screens/welcome_screen.dart';
import 'package:device_preview/device_preview.dart';

void main() {
  runApp(MathQuizApp());
}

class MathQuizApp extends StatelessWidget {
  const MathQuizApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return DevicePreview(
        tools: const [
        DeviceSection(),
    ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        initialRoute: WelcomeScreen.id,
        routes: {
          WelcomeScreen.id: (context) => WelcomeScreen(),
          GameScreen.id: (context) => GameScreen(),
        },
      ),
    );
  }
}